# retroshare_aar

Demostration of how to add RetroShare aar libraries to your flutter project and start the RetroShare service as foreground service. 

## Relevant commits

- https://gitlab.com/elRepo.io/retroshare-arr-import/-/commit/d7ff1db42cce2f2efadb8c0c944577641987ad7c to undestand Android part.
- https://gitlab.com/elRepo.io/retroshare-arr-import/-/commit/fa7ee3fca9e31e8fb4815545f11ab72a7a690fe8 To see how to invoke RS methods
