
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;


const RETROSHARE_HOST = '127.0.0.1';
const RETROSHARE_PORT = 9092;
const RETROSHARE_SERVICE_PREFIX = 'http://$RETROSHARE_HOST:$RETROSHARE_PORT';

String makeAuthHeader(String username, String password) =>
    'Basic ' + base64Encode(utf8.encode('$username:$password'));

Future<Map<String, dynamic>> rsApiCall(
    String path, {
      Map<String, dynamic>? params,
      String? basicAuth,
    }) async {
  final reqUrl = RETROSHARE_SERVICE_PREFIX + path;
  try {
    final response = await http.post(Uri.parse(reqUrl),
        body: jsonEncode(params ?? {}),
        headers: <String, String>{'Authorization': basicAuth ?? ""});


    if (response.statusCode is! int) {
      throw Exception('Request failed: ' + reqUrl);
    }

    switch (response.statusCode) {
      case 200:
        return jsonDecode(utf8.decode(response.bodyBytes));
      default:
        throw Exception("Status code non 200");
    }
  } catch (err) {
    if (err is SocketException || err is http.ClientException) {
      print('Request failed with SocketException or ClientException: ' + reqUrl);
    }
    rethrow;
  }
}

final RETROSHARE_API_USER = 'elrepo.io';

// Creates a Retroshare Account
/// Returns the location to use the account from now on
Future<Map> createLocation(String locationName, String password,
    {String? api_user}) async {
  final mPath = '/rsLoginHelper/createLocationV2';
  final mParams = {
    'locationName': locationName,
    'pgpName': locationName,
    'password': password,
    'apiUser': api_user ?? RETROSHARE_API_USER,
    /* TODO(G10h4ck): The new token scheme permit arbitrarly more secure
       * options to avoid sending PGP password at each request. */
    'apiPass': password
  };
  final response = await rsApiCall(mPath, params: mParams);

  if (!(response is Map)) {
    throw FormatException('response is not a Map');
  } else if (response['retval']['errorNumber'] != 0) {
    throw Exception('Failure creating location: ' + jsonEncode(response));
  } else if (!(response['locationId'] is String)) {
    throw FormatException('location is not a String');
  }

  print('Location done!');

  var location = <String, String>{
    'mLocationName': locationName,
    'mLocationId': response['locationId']
  };

  return location;
}

