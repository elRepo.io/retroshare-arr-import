
import 'package:flutter/services.dart';

import 'android_config.dart';

class RsControl {

  static var isInitialized = false;
  static const String channelName = 'com.example.retroshare_aar/retroshare';
  static const MethodChannel _channel = MethodChannel(channelName);

  static int _androidNotificationImportanceToInt(
      AndroidNotificationImportance importance) {
    switch (importance) {
      case AndroidNotificationImportance.Low:
        return -1;
      case AndroidNotificationImportance.Min:
        return -2;
      case AndroidNotificationImportance.High:
        return 1;
      case AndroidNotificationImportance.Max:
        return 2;
      case AndroidNotificationImportance.Default:
      default:
        return 0;
    }
  }
  static Future<bool> _initialize(
      {FlutterRetroshareServiceAndroidConfig androidConfig =
      const FlutterRetroshareServiceAndroidConfig()}) async {
    isInitialized = await _channel.invokeMethod<bool>('initialize', {
      'android.notificationTitle': androidConfig.notificationTitle,
      'android.notificationText': androidConfig.notificationText,
      'android.notificationImportance': _androidNotificationImportanceToInt(
          androidConfig.notificationImportance),
      'android.notificationIconName': androidConfig.notificationIcon.name,
      'android.notificationIconDefType':
      androidConfig.notificationIcon.defType,
    }) ==
        true;
    // startRsErrorCallback = startRetroShareErrorCallback;
    return isInitialized;
  }

  static Future<bool?> startRetroshare() async {
    if(!RsControl.isInitialized) {
      RsControl.isInitialized = await RsControl._initialize();
    }
    final success =
      await _channel.invokeMethod<bool>('enableBackgroundExecution');
    return success;
  }

  static Future<bool?> stopRetroshare() async {
    print('Stopping Retroshare');
    final success =
      await _channel.invokeMethod<bool>('disableBackgroundExecution');
    return success;
  }
}